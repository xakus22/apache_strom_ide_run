package com.murad.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.storm.Config;
import org.apache.storm.Constants;
import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Murad Salmanov (legenda)
 */
public class WordCount extends BaseBasicBolt {

    private static final Logger logger = LogManager.getLogger(WordCount.class);
    Map<String, Integer> counts = new HashMap<>();

    //Как часто выдавать количество слов
    private Integer emitFrequency;

    public WordCount() {
        emitFrequency = 5; // По умолчанию 60 секунд
    }

    public WordCount(Integer frequency) {
        emitFrequency = frequency;
    }

    // Настраиваем частоту тикового кортежа для этого болта
    // Это доставляет тиковый кортеж на определенном интервале,
    // который используется для запуска определенных действий
    @Override
    public Map<String, Object> getComponentConfiguration() {
        Config config = new Config();
        config.put(Config.TOPOLOGY_TICK_TUPLE_FREQ_SECS, emitFrequency);
        return config;
    }

    @Override
    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        if (tuple.getSourceComponent().equals(Constants.SYSTEM_COMPONENT_ID) &&
                tuple.getSourceStreamId().equals(Constants.SYSTEM_TICK_STREAM_ID)) {
            for (String word : counts.keySet()) {
                Integer count = counts.get(word);
                basicOutputCollector.emit(new Values(word, count));
                logger.info("Emitting a count of " + count + " for word " + word);
            }
        } else {
            String  word  = tuple.getString(0);
            Integer count = counts.get(word);
            if (count == null) count = 0;
            count++;
            counts.put(word, count);
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("word", "count"));
    }
}
