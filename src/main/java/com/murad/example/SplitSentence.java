package com.murad.example;

import org.apache.storm.topology.BasicOutputCollector;
import org.apache.storm.topology.OutputFieldsDeclarer;
import org.apache.storm.topology.base.BaseBasicBolt;
import org.apache.storm.tuple.Fields;
import org.apache.storm.tuple.Tuple;
import org.apache.storm.tuple.Values;

import java.text.BreakIterator;

/**
 * @author Murad Salmanov (legenda)
 */
public class SplitSentence extends BaseBasicBolt {
    //Execute вызывается для обработки кортежей
    @Override
    public void execute(Tuple tuple, BasicOutputCollector basicOutputCollector) {
        //Получить содержимое предложения из кортежа
        String sentence = tuple.getString(0);
        //Итератор для получения каждого слова
        BreakIterator boundary = BreakIterator.getWordInstance();
        //Дайте итератору предложение
        boundary.setText(sentence);
        //Найдите начало первого слова
        int start = boundary.first();
        //Перебирать каждое слово и передавать его в выходной поток
        for (int end = boundary.next(); end != BreakIterator.DONE; start = end, end = boundary.next()) {
            String word = sentence.substring(start, end);
            //Если слово состоит из пробелов, замените его пустым
            word = word.replaceAll("\\s+", "");
            //Если это настоящее слово, то отправить его
            if (!word.equals("")) {
                basicOutputCollector.emit(new Values(word));
            }
        }

    }

    //Объявить, что испускаемые кортежи содержат поле word
    @Override
    public void declareOutputFields(OutputFieldsDeclarer outputFieldsDeclarer) {
        outputFieldsDeclarer.declare(new Fields("word"));
    }
}
